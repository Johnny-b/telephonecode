package com.devcraft.telephonecode

import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.michaelrocks.libphonenumber.android.NumberParseException
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.io.InputStream
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var phoneNumberUtil: PhoneNumberUtil
    private  val TAG = "MainActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        phoneNumberUtil = PhoneNumberUtil.createInstance(this)



        phoneNumberEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(editable: Editable?) {
                val number = editable.toString()
                val PACKAGE_NAME = applicationContext.packageName
                val countryIsoCode = getCountryIsoCode(number)
                countryTextView.text = countryIsoCode ?: "Can't detect a country"

                if (countryIsoCode != null) {
                    try {
                        // get input stream
                        val ims: InputStream = countryIsoCode.let { assets.open(it.toLowerCase(Locale.ROOT) + ".png") }
                        // load image as Drawable
                        val d = Drawable.createFromStream(ims, null)
                        // set image to ImageView
                        ivFlag.setImageDrawable(d)
                    } catch (ex: IOException) {
                        return
                    }
                } else {
                    try {
                        val imsx: InputStream = countryIsoCode.let { assets.open( "ua.png") }
                        val dx = Drawable.createFromStream(imsx, null)
                        ivFlag.setImageDrawable(dx)
                    } catch (ex: IOException) {
                        return
                    }
                }
            }
        })
    }
    private fun getCountryIsoCode(number: String): String? {
        val validatedNumber = if (number.startsWith("+")) number else "+$number"

        val phoneNumber = try {
            phoneNumberUtil.parse(validatedNumber, null)
        } catch (e: NumberParseException) {
            Log.e(TAG, "error during parsing a number")
            null
        }
        if(phoneNumber == null) return null

        return phoneNumberUtil.getRegionCodeForCountryCode(phoneNumber.countryCode)
    }
}